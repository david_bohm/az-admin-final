angular.module('myApp')
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $httpProvider.interceptors.push(function($window, $location, LoopBackAuth) {

            return {

                request: function(config) {

                    var accessToken = $window.sessionStorage['$LoopBack$accessTokenId'];

                    if (accessToken == LoopBackAuth.accessTokenId) {


                    } else {

                        $location.url('/login')

                    }

                    return config;

                },

                response: function(config) {

                    return config;

                }

            }

        });

        $stateProvider.state('login', {
            parent: 'main',
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'UserController'
        });

        $urlRouterProvider.otherwise('/login');

    });

angular.module('myApp')
    .run(function($rootScope, $state, $http, $location, $window) {

        //$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        //
        //    console.log(fromState, toState);
        //
        //    if ($window.sessionStorage['$LoopBack$accessTokenId']) {
        //
        //        console.log('logged');
        //
        //    } else {
        //
        //        // it appears that the actual view transition/transclusion is triggered
        //        // by the $stateChangeSuccess internally so therefore is also prevented
        //        // by notify: false
        //
        //        event.preventDefault();
        //
        //        // As a work around you can trigger the event manually
        //        // with the success callback of the promise returned by
        //        // $state.go()
        //
        //        $state.go('login', null, {notify: false}).then(function(state) {
        //            $rootScope.$broadcast('$stateChangeSuccess', state, null);
        //        });
        //
        //    }
        //
        //});

    });

angular.module('myApp')
    .controller('MainController', function($window, LoopBackAuth) {



});