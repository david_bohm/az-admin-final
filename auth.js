var urlBase = "http://104.131.113.114:3001/api/";

angular.module('myApp')
    .controller('UserController', function($scope, $state, Auth, LoopBackAuth) {

        console.log(localStorage);

        $scope.login = function() {

            Auth.login({

                email: $scope.user.email,
                password: $scope.user.password

            }, function(user) {

                console.log(user);
                $state.go('dashboard');

            }, function(err) {

                console.log(err);

            });
        };
    });

angular.module('myApp')
    .factory('LoopBackAuth', function() {
        var props = ['accessTokenId', 'currentUserId'];
        var propsPrefix = '$LoopBack$';

        function LoopBackAuth() {
            var self = this;
            props.forEach(function(name) {
                self[name] = load(name);
            });
            this.rememberMe = undefined;
            this.currentUserData = null;
        }

        LoopBackAuth.prototype.save = function() {
            var self = this;
            var storage = this.rememberMe ? localStorage : sessionStorage;
            props.forEach(function(name) {
                save(storage, name, self[name]);
            });
        };

        LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
            this.accessTokenId = accessTokenId;
            this.currentUserId = userId;
            this.currentUserData = userData;
        }

        LoopBackAuth.prototype.clearUser = function() {
            this.accessTokenId = null;
            this.currentUserId = null;
            this.currentUserData = null;
        }

        LoopBackAuth.prototype.clearStorage = function() {
            props.forEach(function(name) {
                save(sessionStorage, name, null);
                save(localStorage, name, null);
            });
        };

        return new LoopBackAuth();

        // Note: LocalStorage converts the value to string
        // We are using empty string as a marker for null/undefined values.
        function save(storage, name, value) {
            var key = propsPrefix + name;
            if (value == null) value = '';
            storage[key] = value;
        }

        function load(name) {
            var key = propsPrefix + name;
            return localStorage[key] || sessionStorage[key] || null;
        }
    });

angular.module('myApp')
    .factory('Auth', function($cookies, LoopBackAuth, $http) {
        return {
            login: function(data, cb) {
                var self = this;
                LoopBackAuth.currentUserId = LoopBackAuth.accessTokenId = null;
                $http.post(urlBase + '/users/login?include=user', {
                    email: data.email,
                    password: data.password
                })
                    .then(function(response) {
                        if (response.data && response.data.id) {
                            LoopBackAuth.currentUserId = response.data.userId;
                            LoopBackAuth.accessTokenId = response.data.id;
                        }
                        if (LoopBackAuth.currentUserId === null) {
                            delete $cookies['access_token'];
                            LoopBackAuth.accessTokenId = null;
                        }
                        LoopBackAuth.save();
                        if (LoopBackAuth.currentUserId && response.data && response.data
                                .user) {
                            self.currentUser = response.data.user;
                            cb(self.currentUser);

                        } else {
                            cb({});
                        }
                    }, function() {
                        console.log('User.login() err', arguments);
                        LoopBackAuth.currentUserId = LoopBackAuth.accessTokenId =
                            null;
                        LoopBackAuth.save();
                        cb({});
                    });
            },

            logout: function() {
                LoopBackAuth.clearUser();
                LoopBackAuth.save();
                window.location = '/auth/logout';
            },

            ensureHasCurrentUser: function(cb) {
                var self = this;
                if ((!this.currentUser || this.currentUser.id === 'social') &&
                    $cookies.access_token) {
                    LoopBackAuth.currentUserId = LoopBackAuth.accessTokenId = null;
                    $http.get('/auth/current')
                        .then(function(response) {
                            if (response.data.id) {
                                LoopBackAuth.currentUserId = response.data.id;
                                LoopBackAuth.accessTokenId = $cookies.access_token.substring(
                                    2, 66);
                            }
                            if (LoopBackAuth.currentUserId === null) {
                                delete $cookies['access_token'];
                                LoopBackAuth.accessTokenId = null;
                            }
                            LoopBackAuth.save();
                            self.currentUser = response.data;
                            var profile = self.currentUser && self.currentUser.profiles &&
                                self.currentUser.profiles.length && self.currentUser.profiles[
                                    0];
                            if (profile) {
                                self.currentUser.name = profile.profile.name;
                            }
                            cb(self.currentUser);
                        }, function() {
                            console.log('User.getCurrent() err', arguments);
                            LoopBackAuth.currentUserId = LoopBackAuth.accessTokenId =
                                null;
                            LoopBackAuth.save();
                            cb({});
                        });
                } else {
                    console.log('Using cached current user.');
                    cb(self.currentUser);
                }
            }
        };
    });