var myApp = angular.module('myApp');

var urlBase = "http://104.131.113.114:3001/api/";

myApp.directive("upload", function() {
  return {
    restrict: 'EA',
    templateUrl: 'fields/fileUpload.html',
    controller: function($rootScope, $scope) {

      $scope.onFileSelect = function($files) {

        console.log("File/s selected", $files);

        $scope.files = $files;

        console.log($scope.files);

      };

      $scope.upload = function() {

        var formData = new FormData();

        // formData.append("categoryId", $scope.entry.values['id']);
        console.log($scope.files);

        for (var index in $scope.files) {
          console.log(index);
          var file = $scope.files[index];
          formData.append('file', file);
        }

        $.ajax({

          url: urlBase + 'containers/' + $scope.entry.values['id'] + '/upload',
          type: 'POST',
          data: formData,
          processData: false,  // tell jQuery not to process the data
          contentType: false,  // tell jQuery not to set contentType
          success : function(data) {
            console.log(data);
            $rootScope.$broadcast('image.added');
          }

        });

      }

    }

  }

});

myApp.directive('imagenes', function() {
  return {

    restrict: 'EA',
    templateUrl: 'fields/images.html',
    controller: function($scope, $http) {

      $scope.hovered = false;

      loadImages();

      $scope.removeImage = function(imageId) {

          $http.delete(urlBase + 'containers/' + $scope.entry.values['id'] + '/files/' + imageId)
              .success(function(data) {
                  console.log(data);
                  loadImages();
              })
              .error(function(err) {
                  console.log(err);
              });

      };

      $scope.setPortada = function(index, imageId) {

          $scope.isPortada = true;

      };

      function loadImages() {
          $http.get(urlBase + 'containers/' + $scope.entry.values['id'] + '/files')
              .success(function(files) {
                  $scope.entry.values['images'] = files;
              })
              .error(function(err) {
                  console.error(err);
              });
      }

      $scope.$on('image.added', function(evt) {
          loadImages();
      });

      // console.log($scope.$parent);
    }

  }
});

myApp.directive('search', function() {

    return {

        restrict: 'E',
        templateUrl: 'fields/search.html',
        link: function(scope, elem, attrs) {

            console.log(scope);

            scope.placeholder = attrs.placeholder;


        }
    }

});

myApp.directive('subdomain', function() {

    return {

        restrict: 'E',
        templateUrl: 'fields/subdomain.html',
        controller: function($scope, $http) {

            var getSubdomain = function() {

                $http.get(urlBase + $scope.entry._entityName + "/" + $scope.entry.values['id'] + "/subdomain")
                    .success(function (data) {
                        console.log('sub', data);

                        if (data.name) {

                            $scope.hasSubdomain = true;

                            $scope.subdomain = data.name;
                        }


                    })
                    .error(function(data) {
                        console.log('sub', data);
                    })

            };

            $scope.addSubdomain = function() {

                    $http.post(urlBase + $scope.entry._entityName + "/" + $scope.entry.values['id'] + "/subdomain", {
                        name: $scope.subdomain
                    })
                        .success(function (data) {
                            console.log(data);
                        })
                        .error(function(data) {
                            console.log(data);
                        });
                };

            $scope.editSubdomain = function() {

                $http.put(urlBase + $scope.entry._entityName + "/" + $scope.entry.values['id'] + "/subdomain", {
                    name: $scope.subdomain
                })
                    .success(function (data) {
                        console.log(data);
                    })
                    .error(function(data) {
                        console.log(data);
                    });
            };

            getSubdomain();

        }
    }

});


myApp.directive('showSubdomain', function() {

    return {

        restrict: 'E',
        templateUrl: 'fields/show-subdomain.html',
        controller: function($scope, $http) {

            $scope.subdomain = null;

            var getSubdomain = function() {

                $http.get(urlBase + $scope.entry._entityName + "/" + $scope.entry.values['id'] + "/subdomain")
                    .success(function (data) {
                        console.log('sub', data);
                        $scope.subdomain = data.name;
                    })
                    .error(function(data) {
                        console.log('sub', data);
                    })

            };

            getSubdomain();

        }
    }

});

