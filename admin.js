var myApp = angular.module('myApp', ['ng-admin', 'ngCookies']);
myApp.config(['NgAdminConfigurationProvider', function (nga) {
  // create an admin application
  var urlBase = "http://104.131.113.114:3001/api/";

  var admin = nga.application('az Admin Panel')
    .baseApiUrl(urlBase); // main API endpoint

    // create entities

    // Rubros
    var category = nga.entity('categories').label('Rubros');

    category.listView()
        .perPage(13)
    .fields([
        nga.field('name').isDetailLink(true).detailLinkRoute("show").label('Nombre'),
        nga.field('description','text').label('Descripción')
      ])
    .filters([
            nga.field('name', 'template')
                .label('')
                .pinned(true)
                .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Search" class="form-control"></input><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span></div>')
            ])
    .listActions(['edit','delete'])
    .title('Rubros AZ');

    category.showView().fields([
    	nga.field('description', 'text').label('Descripción'),
    	nga.field('subcategories', 'referenced_list')
            .targetEntity(nga.entity('subcategories'))
            .targetReferenceField('categoryId')
            .targetFields([
              nga.field('name')
            ])
            .sortField('name')
            .sortDir('DESC')
            .label('Sub Rubros'),
        nga.field('images', 'template')
            .label('Imágenes actuales')
            .template('<imagenes></imagenes>'),
        nga.field('subdomain', 'template')
            .label('Subdominio')
            .template('<show-subdomain></show-subdomain>')

    ]).title('Rubro "{{ entry.values.name }}"');

    category.creationView().fields([
    	nga.field('name').label('Nombre'),
    	nga.field('description','text').label('Descripción')
    ]).title('Nuevo Rubro');

    category.editionView().fields([
        nga.field('name', 'text').label('Nombre'),
        nga.field('description', 'text').label('Descripción'),
        nga.field('images', 'template')
            .label('Imágenes actuales')
            .template('<imagenes></imagenes>'),
        nga.field('file_upload', 'template')
            .label('Imágenes')
            .template('<upload></upload>'),
        nga.field('subdomain', 'template')
            .label('Subdominio')
            .template('<subdomain></subdomain>'),
    ]).title('Editar Rubro "{{ entry.values.name }}"');

    category.deletionView().fields(
        nga.field('name').label('Nombre')
    ).title('Borrar Rubro "{{ entry.values.name }}"');

    admin.addEntity(category);

    // Sub Rubros
    var subcategory = nga.entity('subcategories').label('Sub Rubros');

    subcategory.listView()
      .perPage(15)
      .fields([
          nga.field('name').isDetailLink(true).detailLinkRoute("show").label('Nombre'),
          nga.field('categoryId', 'reference')
            .targetEntity(category)
            .targetField(nga.field('name'))
            .label('Rubro')
        ])
        .filters([
            nga.field('name', 'template')
                .label('')
                .pinned(true)
                .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Search" class="form-control"></input><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span></div>')
        ])
    .listActions(['edit','delete'])
    .title('Sub Rubros AZ');

    subcategory.showView().fields([
        nga.field('description', 'text').label('Descripción'),
        nga.field('images', 'template')
            .label('Imágenes actuales')
            .template('<imagenes></imagenes>'),
      nga.field('entities', 'referenced_list')
        .targetEntity(nga.entity('entities'))
        .targetReferenceField('subcategoryId')
        .detailLinkRoute('show')
        .targetFields([
          nga.field('name'),
          nga.field('description')
        ])
        .sortField('id')
        .sortDir('DESC')
        .label('Entidades'),
        nga.field('subdomain', 'template')
            .label('Subdominio')
            .template('<show-subdomain></show-subdomain>')

    ]).title('Sub Rubro "{{ entry.values.name }}"');

    subcategory.creationView().fields([
      nga.field('name').label('Nombre'),
      nga.field('description','text').label('Descripción'),
      nga.field('categoryId', 'reference')
        .targetEntity(category)
        .targetField(nga.field('name'))
        .remoteComplete(true)
        .label('Rubro')
    ]).title('Nuevo Sub Rubro');

    subcategory.editionView().fields([
            nga.field('name', 'text').label('Nombre'),
            nga.field('description', 'text').label('Descripción'),
            nga.field('images', 'template')
                .label('Imágenes actuales')
                .template('<imagenes></imagenes>'),
            nga.field('file_upload', 'template')
                .label('Imágenes')
                .template('<upload></upload>'),
            nga.field('subdomain', 'template')
                .label('Subdominio')
                .template('<subdomain></subdomain>')

        ]

    ).title('Editar Sub Rubro "{{ entry.values.name }}"');

    subcategory.deletionView().fields(
        nga.field('name').label('Nombre')
    ).title('Borrar Sub-Rubro "{{ entry.values.name }}"');

    admin.addEntity(subcategory);

    // Entidades
    var entity = nga.entity('entities').label('Entidades');

    entity.listView()
      .perPage(15)
      .fields([
          nga.field('name').isDetailLink(true).detailLinkRoute("show").label('Nombre'),
          nga.field('subcategoryId', 'reference')
            .targetEntity(subcategory)
            .targetField(nga.field('name'))
            .label('Sub Rubro')
    ]).filters([
            nga.field('name', 'template')
                .label('')
                .pinned(true)
                .template('<search id="1" placeholder="Búsqueda por nombre"></search>'),
            nga.field('entity', 'template')
                .label('')
                .pinned(true)
                .template('<search list="true" id="2" placeholder="Búsqueda por sub rubro"></search>')
        ])
        .listActions(['edit', 'delete'])
        .title('Entidades');

    entity.showView().fields([
    	nga.field('description', 'text').label('Descripción'),
        nga.field('images', 'template')
            .label('Imágenes actuales')
            .template('<imagenes></imagenes>'),
    	nga.field('subcategoryId', 'reference')
        .targetEntity(subcategory)
        .targetField(nga.field('name'))
        .detailLinkRoute('show')
        .label('Sub Rubro'),
    	nga.field('default_phone').label('Teléfono'),
    	nga.field('default_email').label('Email'),
    	nga.field('website').label('WEB'),
    	nga.field('facebook').label('Facebook'),
    	nga.field('twitter').label('Twitter'),
        nga.field('subdomain', 'template')
            .label('Subdominio')
            .template('<show-subdomain></show-subdomain>')

            ]).title('Entidad "{{ entry.values.name }}"');

    entity.creationView().fields([
      nga.field('name').label('Nombre'),
      nga.field('description','text').label('Descripción'),
      nga.field('subcategoryId', 'reference')
      .targetEntity(subcategory)
      .targetField(nga.field('name'))
      .remoteComplete(true)
      .label('Sub Rubro')
      ]).title('Nueva Entidad');

      entity.editionView().fields(
        [
            nga.field('description', 'text').label('Descripción'),
            nga.field('images', 'template')
                .label('Imágenes actuales')
                .template('<imagenes></imagenes>'),
            nga.field('default_phone').label('Teléfono'),
            nga.field('default_email').label('Email'),
            nga.field('website').label('WEB'),
            nga.field('facebook').label('Facebook'),
            nga.field('twitter').label('Twitter'),
            nga.field('file_upload', 'template')
                .label('Imágenes')
                .template('<upload></upload>'),
            nga.field('subdomain', 'template')
                .label('Subdominio')
                .template('<subdomain></subdomain>')
        ]
      ).title('Editar Entidad "{{ entry.values.name }}"');

     entity.deletionView().fields(
        nga.field('name').label('Nombre')
     ).title('Borrar Entidad "{{ entry.values.name }}"');

    admin.addEntity(entity);

    // Adverts

    var advert = nga.entity('adverts').label('Promociones');

    advert.listView().fields([
        nga.field('title').isDetailLink(true).detailLinkRoute("show").label('Título'),
        nga.field('description', 'text').label('Descripción'),
        nga.field('from_date','date').label('Desde'),
        nga.field('to_date','date').label('Hasta'),
        nga.field('entityId', 'reference')
            .targetEntity(entity)
            .targetField(nga.field('name'))
            .label('Entidad')
    ])
        .filters([
            nga.field('title', 'template')
                .label('')
                .pinned(true)
                .template('<search placeholder="Búsqueda por nombre"></search>'),
            nga.field('entity', 'template')
                .label('')
                .pinned(true)
                .template('<search placeholder="Búsqueda por entidad"></search>')
        ])
    .title('Promociones');

    advert.creationView().fields([
        nga.field('title').label('Título'),
        nga.field('description', 'text').label('Descripción'),
        nga.field('from_date','date').label('Desde'),
        nga.field('to_date','date').label('Hasta'),
        nga.field('entityId', 'reference')
            .targetEntity(entity)
            .targetField(nga.field('name'))
            .label('Entidad')
    ]).title('Promociones');

    advert.showView().fields([
        nga.field('title').label('Título'),
        nga.field('description', 'text').label('Descripción'),
        nga.field('from_date','date').label('Desde'),
        nga.field('to_date','date').label('Hasta'),
        nga.field('entityId', 'reference')
            .targetEntity(entity)
            .targetField(nga.field('name'))
            .detailLinkRoute('show')
            .label('Entidad')
    ]).title('Promociones');

    advert.deletionView().fields(
        nga.field('title').label('Titulo')
    ).title('Borrar Promocion "{{ entry.values.title }}"');

    admin.addEntity(advert);

    // attach the admin application to the DOM and execute it
    nga.configure(admin);
}]);

myApp.config(['RestangularProvider', function (RestangularProvider) {

  var initInjector = angular.injector(['ng']);
  var $http = initInjector.get('$http');

  RestangularProvider.setFullResponse(true);

  RestangularProvider.addFullRequestInterceptor(function(element, operation, what, url, headers, params, httpConfig) {

      if (operation == 'getList' && params._filters) {

          console.log('search');

          var offset = (params._page - 1) * params._perPage;
          var perPage = params._perPage;

          if (params._filters['name']) {

              var query = params._filters['name'];

              params.filter = {
                  where: {
                      name: {
                          like: query
                      }
                  },
                  limit: perPage.toString(),
                  offset: offset.toString()
              };

              delete params._filters;
              delete params._page;
              delete params._perPage;
              delete params._sortField;
              delete params._sortDir;

          } else if (params._filters['title']) {

              var query = params._filters['title'];

              params.filter = {
                  where: {
                      title: {
                          like: query
                      }
                  },
                  limit: perPage.toString(),
                  offset: offset.toString()
              };

              delete params._filters;
              delete params._page;
              delete params._perPage;
              delete params._sortField;
              delete params._sortDir;

          } else if (params._filters['entity']) {

              var entityName = params._filters['entity'];

              $http.get(urlBase + "entities", {
                  params: {
                      filter: {
                          where: {
                              name: {
                                  like: entityName
                              }
                          }
                      }
                  }
              })
                  .success(function(data) {
                      console.log(data);
                      var entityId = data[0].id;

                      //params.filter = {
                      //    where: {
                      //        entityId: entityId
                      //    },
                      //    limit: perPage.toString(),
                      //    offset: offset.toString()
                      //};
                      //
                      //delete params._filters;
                      //delete params._page;
                      //delete params._perPage;
                      //delete params._sortField;
                      //delete params._sortDir;


                  })
                  .error(function(err) {
                      console.log(err);
                  });

              console.log(entityId);

              params.filter = {
                  where: {
                      entityId: entityId
                  },
                  limit: perPage.toString(),
                  offset: offset.toString()
              };

              delete params._filters;
              delete params._page;
              delete params._perPage;
              delete params._sortField;
              delete params._sortDir;

          } else {

              params.filter = {
                  where: {

                  }
              };

              for (var key in params._filters) {
                  params.filter.where[key] = params._filters[key];
                  console.log(1, params.filter);
              }

              delete params._filters;
              delete params._page;
              delete params._perPage;
              delete params._sortField;
              delete params._sortDir;

          }

      } else if  (operation == 'getList') {

          console.log(params._filters);

          var offset = (params._page - 1) * params._perPage;
          var perPage = params._perPage;

          delete params._filters;
          delete params._page;
          delete params._perPage;
          delete params._sortField;
          delete params._sortDir;

          params.filter = {"limit":perPage.toString(),"skip":offset.toString()};
      }

    return { params: params };
      
  });

  /*
    RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response) {
    var count = response.headers('X-Total-Count');
    console.log("Headers: ", response.headers());
    return data;
    });
  */

}]);


